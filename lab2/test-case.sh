# test rule 1
winpty docker exec -it case_boss_1 curl -x boss:aaaaaa@192.168.1.1:3128 192.168.1.2/music.mp3
winpty docker exec -it case_boss_1 curl -x boss:aaaaaa@192.168.1.1:3128 https://kwejk.pl/

# test rule 2
winpty docker exec -it case_manager_1 curl -x manager:aaaaaa@192.168.1.1:3128 https://kwejk.pl/

# test rule 3
winpty docker exec -it case_projectant_1 curl -x projectant1:aaaaaa@192.168.1.1:3128 192.168.1.2/music.mp3
winpty docker exec -it case_projectant_1 curl -x projectant1:aaaaaa@192.168.1.1:3128 192.168.1.2/text.txt
winpty docker exec -it case_projectant_1 curl -x projectant1:aaaaaa@192.168.1.1:3128 192.168.1.2/executable.exe
winpty docker exec -it case_projectant_1 curl -x projectant1:aaaaaa@192.168.1.1:3128 http://www.download.windowsupdate.com/msdownload/update/v3-19990518/cabpool/sbs2003-kb833992-v3-x86-plk_0ba1da9a5eb1ab22c2e61962d35a1d2f47043ca2.exe

# test rule 4
winpty docker exec -it case_programmer_1 curl -x programmer1:aaaaaa@192.168.1.1:3128 https://kwejk.pl/
winpty docker exec -it case_programmer_1 curl -x programmer1:aaaaaa@192.168.1.1:3128 https://www.interia.pl/
winpty docker exec -it case_programmer_1 curl -x programmer1:aaaaaa@192.168.1.1:3128 https://www.o2.pl/

# test rule 5
winpty docker exec -it case_admin_1 curl -x admin:aaaaaa@192.168.1.1:3128 https://kwejk.pl/

# test rule 6
winpty docker exec -it case_accounting_1 curl -x accounting1:aaaaaa@192.168.1.1:3128 https://kwejk.pl/

winpty docker exec -it case_accounting_1 curl -x accounting1:aaaaaa@192.168.1.1:3128 http://www.robotstxt.org/robots.txt
winpty docker exec -it case_accounting_1 curl -x accounting1:aaaaaa@192.168.1.1:3128 https://upload.wikimedia.org/wikipedia/commons/0/03/Linus-linux.ogg

winpty docker exec -it case_accounting_1 curl -x accounting1:aaaaaa@192.168.1.1:3128 192.168.1.2
winpty docker exec -it case_accounting_1 curl -x accounting1:aaaaaa@192.168.1.1:3128 192.168.1.2/music.mp3
winpty docker exec -it case_accounting_1 curl -x accounting1:aaaaaa@192.168.1.1:3128 192.168.1.2/text.txt
